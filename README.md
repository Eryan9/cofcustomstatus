# Chroniques Oubliées Fantasy - Custom Status  
  
Ce module ajoute les bonus/malus d'effet sur les statuts par défaut  
  
Il suffit de faire un clic droit sur un token et d'assigner le statut désiré.  
  
## Statuts mis à jour  
  
### ***Bénédiction***  
Ajoute les bonus/malus suivant :  
  
- FOR +2 (+1 mod)  
- DEX +2 (+1 mod)  
- CON +2 (+1 mod)  
- INT +2 (+1 mod)  
- SAG +2 (+1 mod)  
- CHA +2 (+1 mod)  
- CONTACT +1  
- DISTANCE +1  
- MAGIQUE +1  

### ***Renversé***  
Ajoute les bonus/malus suivant :  
  
- CONTACT -5  
- DISTANCE -5  
- MAGIQUE -5  
- DEF -5  

### ***Aveuglé***  
Ajoute les bonus/malus suivant :  
  
- CONTACT -5  
- DISTANCE -10  
- MAGIQUE -5  
- INIT -5  
  
### ***Etourdi***  
Ajoute les bonus/malus suivant :  
  
- DEF -5  

