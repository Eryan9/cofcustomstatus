import {EffectsModifications} from "../effects/effects.js";

// A l'init du monde
Hooks.once("init",async function(){
   /// Parcours des modifications
    for(let modificationId in EffectsModifications){
        // Recherche du status correspondant à la modification
        let status = CONFIG.statusEffects.find(eff=>eff.id === modificationId);
        // Si le statut a été trouvé
        // Modification du libellé et ajout de l'effet (des modificateurs) si nécessaire
        if (status){
            // Récupération de l'objet de modification concerné
            let modifications = EffectsModifications[modificationId];
            
            if (modifications.label) status.label = modifications.label;
            if (modifications.changes) status.changes = modifications.changes;
        }
    }
});