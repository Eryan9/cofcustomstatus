export const EffectsModifications = { 
    "bless":{
        label:"COFCUSTOMSTATUS.bless",
        changes:[
			{
				key: "data.stats.str.bonus",
				mode: 2,
				value: 2
			},

			{
				key: "data.stats.dex.bonus",
				mode: 2,
				value: 2
			},                       

			{
				key: "data.stats.con.bonus",
				mode: 2,
				value: 2
			},                      

			{
				key: "data.stats.int.bonus",
				mode: 2,
				value: 2
			},                      

			{
				key: "data.stats.wis.bonus",
				mode: 2,
				value: 2
			},                       

			{
				key: "data.stats.cha.bonus",
				mode: 2,
				value: 2
			},         

			{
				key: "data.attacks.melee.bonus",
				mode: 2,
				value: 1
			},        

			{
				key: "data.attacks.ranged.bonus",
				mode: 2,
				value: 1
			},
         

			{
				key: "data.attacks.magic.bonus",
				mode: 2,
				value: 1
			},
          
		]        
    },
    "prone":{
        label:"COFCUSTOMSTATUS.prone",
        changes:[
			{
				key: "data.attacks.melee.bonus",
				mode: 2,
				value: -5
			},       

			{
				key: "data.attacks.ranged.bonus",
				mode: 2,
				value: -5
			},           

			{
				key: "data.attacks.magic.bonus",
				mode: 2,
				value: -5
			},

			{
				key: "data.attributes.def.bonus",
				mode: 2,
				value: -5
			}
		]
    },
    "blind":{
        label:"COFCUSTOMSTATUS.blind",
        changes:[
			{
				key: "data.attacks.melee.bonus",
				mode: 2,
				value: -5
			},

			{
				key: "data.attacks.ranged.bonus",
				mode: 2,
				value: -10
			},

			{
				key: "data.attacks.magic.bonus",
				mode: 2,
				value: -5
			},          

			{
				key: "data.attributes.init.bonus",
				mode: 2,
				value: -5
			}          		
		]        
    },
    "stun":{
        label:"COFCUSTOMSTATUS.stun",
        changes:[
			{
				key: "data.attributes.def.bonus",
				mode: 2,
				value: -5
			}
		]        
    },
	"magicShield":{
		label: "COFCUSTOMSTATUS.magicShield",
		changes: [
			{
				key: "data.attributes.def.bonus",
				mode: 2,
				value: 4
			}
		]
	}
}